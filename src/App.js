import { useState, useEffect } from 'react';

import AppNavBar from './components/AppNavBar';

import Home from './pages/Home';
import Register from './pages/Register';
import AllProducts from './pages/ProdAll';
import Catalog from './pages/Products';
import Login from './pages/Login';
import ProdView from './pages/ProdView';
import UserView from './pages/UserView';
import AddProduct from './pages/AddProduct';
import Update from './pages/UpdateProd';
import NewPass from './pages/Password';
import Logout from './pages/Logout';
import Error from './pages/Error';

import './App.css'

import { UserProvider } from './UserContext';

import {BrowserRouter as Router, Routes, Route}from 'react-router-dom';

function App() {

  const [user, setUser] = useState({
      id: null,
      isAdmin: null
    }); 
  const unsetUser = () => {

    localStorage.clear();
    setUser({
      id: null,
      isAdmin: null
    });
  }

  useEffect(() => {

    let token = localStorage.getItem('access');

    fetch('https://evening-caverns-96869.herokuapp.com/users/details', {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    .then(res => res.json())
    .then(convertedData => {
      if (typeof convertedData._id !== "undefined") {
        setUser({
          id: convertedData._id,
          isAdmin: convertedData.isAdmin
        });
      } else {
        setUser({
          id: null,
          isAdmin: null
        });
      }
    });

  },[user]);

  return (
        <div className="container-fluid">
          <UserProvider value={ {user, setUser, unsetUser} } >
            <Router>
              <AppNavBar />
              <Routes>
                <Route path='/' element={<Home />} />
                <Route path='/register' element={<Register />} />
                <Route path='/products/all' element={<AllProducts />} />
                <Route path='/products' element={<Catalog />} />
                <Route path='/login' element={<Login />}/>
                <Route path='/products/view/:id' element={<ProdView />} />
                <Route path='/products/view2/:id' element={<UserView />} />
                <Route path='/create' element={<AddProduct />} />
                <Route path='/products/update/:id' element={<Update />} />
                <Route path='/password' element={<NewPass />} />
                <Route path='/logout' element={<Logout />} />
                <Route path='*' element={<Error />} />
              </Routes>
            </Router>            
          </UserProvider>
        </div>
  );
}

export default App;
