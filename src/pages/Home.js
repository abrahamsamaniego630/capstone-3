import { useContext } from 'react';

import UserContext from '../UserContext';

import Product from './Products';

import AdminView from './../components/AdminView';

export default function Home() {
	const { user } = useContext(UserContext);

	return(
		<>
			<div>
				{
					user.isAdmin !== true ?
						<Product />
					:
						<AdminView />
				}
			</div>
		</>
	);
};