import { useState, useEffect, useContext } from 'react';
import {Container, Form, Button} from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

import Hero from '../components/Banner';

const data = {
	title: 'Welcome to the Register Page',
	content: 'Create an account to enroll'
}

export default function Register() {
	const { user } = useContext(UserContext);

	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [middleName, setMiddleName] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	const [isActive, setIsActive] = useState(false);
	const [isMatched, setIsMatched] = useState(false);
	const [isValid, setIsValid] = useState(false);
	const [isAllowed, setIsAllowed] = useState(false);

	useEffect(() => {
		if (mobileNo.length === 11) 
		{
			setIsValid(true);
			if (
				(password1 === password2) && (password1 !== '' && password2 !== '')
			) {
				setIsMatched(true);
				if (firstName !== '' && lastName !== '' && email !== '') {
					setIsActive(true);
					setIsAllowed(true);
				} else {
					setIsActive(false);
					setIsAllowed(false);
				}
			} else {
				setIsActive(false);
				setIsMatched(false);
				setIsAllowed(false);
			}
		} else if (
			(password1 !== '' && password2 !== '') && (password1 === password2)
		 	) {
				setIsMatched(true);
		} else {
			setIsActive(false);
			setIsMatched(false);
			setIsValid(false);
			setIsAllowed(false);
		};
	},[firstName, lastName, middleName, email, mobileNo, password1, password2]);

	const registerUser = async(eventSubmit) => {
		eventSubmit.preventDefault()
		const isRegistered = await fetch('https://evening-caverns-96869.herokuapp.com/users/register', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
			    firstName: firstName,
			    lastName: lastName,
			    middleName: middleName,
			    email: email,
			    password: password1,
			    mobileNo: mobileNo
			})
		}).then(response => response.json())
		.then(data => {
			if (data.email) {
				return true;
			} else {
				return false;
			}
		})
		if (isRegistered) {
			setFirstName('');
			setLastName('');
			setMiddleName('');
			setEmail('');
			setMobileNo('');
			setPassword1('');
			setPassword2('');

			await Swal.fire({
				icon: 'success',
				title: 'Registration Successful',
				text: 'Thank you for creating an account.' 
			});
			window.location.href = "/login";

		} else {
			Swal.fire({
				icon: 'error',
				title: 'Something Went Wrong',
				text: 'Your email might already be in use. Please use another email.'
			});
		}
	};

	return(
		// Redirect this to User View page once done
		user.id
		?
			<Navigate to="/courses" replace={true} />
		:
		<>
			<Hero bannerData={data} />
			<Container className="form__reg">
				{/*Form Heading*/}
				{
					isAllowed ?
						<h1 className="text-center text-success">You May Now Register</h1>
					:
						<h1 className="text-center">Register Form</h1>
				}
				<h6 className="mt-3 text-center text-secondary">Fill Up the Form Below</h6>

				<Form onSubmit={e => registerUser(e)}>
					{/*First Name Field*/}
					<Form.Group>
						<Form.Label>First Name: </Form.Label>
						<Form.Control type="text" placeholder="Enter your first name" value={firstName} onChange={event => setFirstName(event.target.value)} required />
					</Form.Group>
					{/*Last Name Field*/}
					<Form.Group>
						<Form.Label>Last Name: </Form.Label>
						<Form.Control type="text" placeholder="Enter your last name" value={lastName} onChange={event => setLastName(event.target.value)} required />
					</Form.Group>
					{/*Middle Name Field*/}
					<Form.Group>
						<Form.Label>Middle Name: </Form.Label>
						<Form.Control type="text" placeholder="Enter your middle name" value={middleName} onChange={event => setMiddleName(event.target.value)} required />
					</Form.Group>
					{/*Email Address Field*/}
					<Form.Group>
						<Form.Label>Email: </Form.Label>
						<Form.Control type="email" placeholder="Enter your email" value={email} onChange={event => setEmail(event.target.value)} required />
					</Form.Group>
					{/*Mobile Number Field*/}
					{/*For Capstone 3: customize this component so that you will get the correct format for the mobile number*/}
					<Form.Group>
						<Form.Label>Mobile No.: </Form.Label>
						<Form.Control type="number" placeholder="Enter your mobile no. here" value={mobileNo} onChange={event => setMobileNo(event.target.value)} required />
							{
								isValid ?
									<span className="text-success">Mobile Number is Valid!</span>
								:
									<span className="text-danger">Mobile Number Should Be 11-digits!</span>
							}
					</Form.Group>
					{/*Password Field*/}
					<Form.Group>
						<Form.Label>Password: </Form.Label>
						<Form.Control type="password" minlength="8" maxlength="16" placeholder="Enter your password" value={password1} onChange={event => setPassword1(event.target.value)} required />
					</Form.Group>
					{/*Confirm Password Field*/}
					<Form.Group>
						<Form.Label>Confirm Password: </Form.Label>
						<Form.Control type="password" minlength="8" maxlength="16" placeholder="Confirm your password" value={password2} onChange={event => setPassword2(event.target.value)} required />
							{
								isMatched ?
									<span className="text-success">Passwords Matched!</span>
								:
									<span className="text-danger">Passwords should match!</span>
							}
					</Form.Group>

					{/*Register Button*/}
					{
						isActive ? 
							<Button className="mb-3 btn-success btn-block" type="submit"> Register </Button>
						:
							<Button className="mb-3 btn-secondary btn-block" disabled> Register </Button>
					}

				</Form>
			</Container>
		</>
	);
};