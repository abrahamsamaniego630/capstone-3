import { useState, useEffect, useContext } from 'react';
import { Navigate }from 'react-router-dom';

import AdminBanner from './../components/AdminBanner';
import AllCard from './../components/AllCard';

import UserContext from '../UserContext';

export default function AllProducts() {

	let token = localStorage.getItem('access');
	const { user } = useContext(UserContext);

	const [productsCollection, setProductsCollection] = useState([]);

	useEffect(() => {
		fetch('https://evening-caverns-96869.herokuapp.com/products/all', {
			method: 'GET',
			headers: {
				'Authorization': `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(convertedData => {
			setProductsCollection(convertedData.map(prod => {
				return(
					<AllCard key={prod._id} allProp={prod} />
				)
			})) 
		});
	},[token]);
	return(
		<>
			{
				user.isAdmin !== true ?
				<Navigate to="/error" replace={true} />
				:
				<>
					<AdminBanner />
					<div className="container-fluid text-center">
						<div className="row">
							<div className="col">
								{productsCollection}					
							</div>
						</div>
					</div>
				</>
			}

		</>
	)
}