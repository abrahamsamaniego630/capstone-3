import React from 'react';

import { useState, useEffect } from 'react';
import { useParams, Link } from 'react-router-dom';
import { Container, Form, Button } from 'react-bootstrap';

import Swal from 'sweetalert2';

import Hero from '../components/Banner';

const data = {
	title: 'Welcome to the Update Product Page',
	content: 'You can update details for a course here'
}

export default function NewPass() {

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [newpassword, setNewPassword] = useState('');
	const [confirmpass, setConfirmPass] = useState('');
	
	const {id} = useParams();
	let token = localStorage.getItem('access');

	const [isActive, setIsActive] = useState(false);
	const [isMatched, setIsMatched] = useState(false);

	useEffect(() => {
		if ((email !== '' && password !== '' && newpassword !== '' && confirmpass !== '') && (newpassword === confirmpass)
		) {
			setIsActive(true);
			setIsMatched(true);
		} else {
			setIsActive(false);
			setIsMatched(false);
		}
	},[email, password, newpassword, confirmpass, id]);

	const changePass = async (submit) => {
		submit.preventDefault()

		const isUpdated = await fetch(`https://evening-caverns-96869.herokuapp.com/users/upass`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				"email": email,
				"password": password,
				"newpassword": newpassword,
				"confirmpass": confirmpass,
			})
		}).then(response => response.json())
		.then(data => {
			if (data) {
				console.log(data)
				return true;
			} else {
				return false;
			}
		})

		if (isUpdated) {
			setEmail('');
			setPassword('');
			setNewPassword('');
			setConfirmPass('');

			await Swal.fire({
				icon: 'success',
				title: 'Update Successful',
				text: 'Password was successfully updated!' 
			});
			window.location.href = "/logout";
		} else {
			Swal.fire({
				icon: 'error',
				title: 'Something Went Wrong',
				text: 'Please make sure your email and/or password entries are correct!'
			});
		}
	};

	return (
		<>
			<Hero bannerData={data} />
			<Container className="container-fluid form__view">
				<div>
					<h1 className="text-center mt-3">Update Password Form</h1>
					<Form onSubmit={e => changePass(e)}>
						{/*Email Field*/}
						<Form.Group>
							<Form.Label>Email: </Form.Label>
							<Form.Control type="text" placeholder="Enter your email" required value={email} onChange={e => {setEmail(e.target.value)}} />
						</Form.Group>
						{/*Password Field*/}
						<Form.Group>
							<Form.Label>Password: </Form.Label>
							<Form.Control type="password" minlength="8" maxlength="16" placeholder="Enter existing password here" required value={password} onChange={e => {setPassword(e.target.value)}} />
						</Form.Group>
						{/*New Password Field*/}
						<Form.Group>
							<Form.Label>New Password: </Form.Label>
							<Form.Control type="password" minlength="8" maxlength="16" placeholder="Enter new password" required value={newpassword} onChange={e => {setNewPassword(e.target.value)}} />
						</Form.Group>
						{/*Confirm Password Field*/}
						<Form.Group>
							<Form.Label>Confirm Password: </Form.Label>
							<Form.Control type="password" minlength="8" maxlength="16" placeholder="Enter new password" required value={confirmpass} onChange={e => {setConfirmPass(e.target.value)}} />
							{
								isMatched ?
									<span className="text-success">Passwords Matched!</span>
								:
									<span className="text-danger">Passwords should match!</span>
							}
						</Form.Group>
						{/*Update Button*/}
						{
							isActive ? 
								<Button className="mr-3 mb-3 btn-success btn-large card__btn" type="submit"> Update </Button>
							:
								<Button className="mr-3 mb-3 btn-secondary btn-large card__btn" disabled> Update </Button>
						}
						<Link className="btn btn-large btn-success mr-3 mb-3 card__btn" to="/">
							Back to Home
						</Link>
					</Form>							
				</div>
			</Container>
		</>
	);
};