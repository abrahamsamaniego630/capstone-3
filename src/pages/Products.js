import { useState, useEffect }from 'react';
import AdminBanner from './../components/AdminBanner';
import ProdCard from './../components/ProdCard';

export default function Products() {

	const [productsCollection, setProductsCollection] = useState([]);

	useEffect(() => {
		fetch('https://evening-caverns-96869.herokuapp.com/products/active')
		.then(res => res.json())
		.then(convertedData => {
			setProductsCollection(convertedData.map(prod => {
				return(
					<ProdCard key={prod._id} productProp={prod} />
				)
			})) 
		});
	},[]);

	return(
		<>
			<AdminBanner />
			<div className="container-fluid text-center">
				<div className="row">
					<div className="col">
						{productsCollection}					
					</div>
				</div>
			</div>
	
						

		</>
	)
}