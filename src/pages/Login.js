import { useState, useEffect} from 'react';

import Bruce from './../components/Banner';
import {Form, Button, Container} from 'react-bootstrap';
import Swal from 'sweetalert2';

const data = {
	content: "Sign in to your account below"
}

export default function Login(){

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	let addressSign = email.search('@');
	let dns = email.search('.com');

	const [isActive, setIsActive] = useState(false);
	const [isValid, setIsValid] = useState(false);

	useEffect(() => {
		if (dns !== -1 && addressSign !== -1) {
			setIsValid(true);
			if (password!== '') {
				setIsActive(true);
			} else {
				setIsActive(false);
			}
		} else {
			setIsValid(false);
			setIsActive(false);
		}
	},[email, password, dns, addressSign]);

	const loginUser = async (event) => {
		event.preventDefault()
			fetch('https://evening-caverns-96869.herokuapp.com/users/login', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					    "email": email,
					    "password": password
				})
			}).then(res => res.json())
			.then(async data => {
				let token = data.access;

				if (typeof token !== 'undefined') {
					event.preventDefault()
					localStorage.setItem('access', token);

					await Swal.fire({
						icon: 'success',
						title: 'Login Successful',
						text: 'You have successfully logged in.'
					})	
					window.location.href = "/";				
				} else {
					event.preventDefault()
					await Swal.fire({
						icon: 'error',
						title: 'Check Your Credentials',
						text: 'Contact Admin If Problem Persists'
					})						
				}
			})


	};

	return(
		<>
			<Bruce bannerData={data} />
			<Container className="form__login">
				<h1 className="mt-2 text-center"> Login Form </h1>
				<Form onSubmit={e => loginUser(e)}>
					{/*Email Address Field*/}
					<Form.Group>
						<Form.Label>Email: </Form.Label>
						<Form.Control type="email" placeholder="Enter email here" required value={email} onChange={e => {setEmail(e.target.value)} } />
						{
							isValid ?
								<h6 className="text-success"> Email is Valid </h6>
							:
								<h6 className="text-mute"> Email is Invalid </h6>					
						}
					</Form.Group>
					{/*Password Field*/}
					<Form.Group>
						<Form.Label>Password: </Form.Label>
						<Form.Control type="password" placeholder="Enter password here" required value={password} onChange={e => {setPassword(e.target.value)}}  />
					</Form.Group>
					{
						isActive ?
							<Button variant="success" type="submit" className="mb-3 btn-block"> Login </Button>
						:
							<Button variant="secondary" type="submit" className="mb-3 btn-block" disabled> Login </Button>
					}
				</Form>
			</Container>
		</>
	);
};