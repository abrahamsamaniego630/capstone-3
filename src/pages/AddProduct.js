import { useState, useEffect, useContext } from 'react';
import { Container, Form, Button, InputGroup } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';

import UserContext from '../UserContext';
import Hero from '../components/Banner';

const data = {
	content: 'You can add a new product to expand your list.'
}

export default function AddProduct() {

	const { user } = useContext(UserContext);

	let token = localStorage.getItem('access');

	const [name, setName] = useState('');
	const [description, setDesc] = useState('');
	const [price, setPrice] = useState('');

	const [isActive, setIsActive] = useState(false);
	const [isAllowed, setIsAllowed] = useState(false);

	useEffect(() => {
		if (name !== '' && description !== '' && price !== '') 
		{
			setIsActive(true);
			setIsAllowed(true);
		} else {
			setIsActive(false);
			setIsAllowed(false);
		};
	},[name, description, price]);

	const newProd = async(eventSubmit) => {
		eventSubmit.preventDefault()
		const isAdded = await fetch('https://evening-caverns-96869.herokuapp.com/products/create', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
			    name: name,
			    description: description,
			    price: price
			})
		}).then(response => response.json())
		.then(data => {
			if (data) {
				return true;
			} else {
				return false;
			}
		})
		if (isAdded) {
			setName('');
			setDesc('');
			setPrice('');

			await Swal.fire({
				icon: 'success',
				title: 'Successfully Added!',
				text: 'You have successfully added a new product to the list!' 
			});
			window.location.href = "/products";

		} else {
			Swal.fire({
				icon: 'error',
				title: 'Something Went Wrong',
				text: 'Please try again later.'
			});
		}
	};

	return(
		<>
			{
				user.isAdmin !== true ?
					<Navigate to="/error" replace={true} />
				:
					<>
					<Hero bannerData={data} />
					<Container className="my-3 form__view">
						{/*Form Heading*/}
						{
							isAllowed ?
								<h1 className="text-center text-success">You May Now Add the New Product</h1>
							:
								<h1 className="text-center">New Product Form</h1>
						}
						<h6 className="mt-3 text-center text-secondary">Fill Up the Form Below</h6>

						<Form onSubmit={e => newProd(e)}>
							{/*Product Name Field*/}
							<Form.Group>
								<Form.Label>Product Name: </Form.Label>
								<Form.Control type="text" placeholder="Enter your product name" value={name} onChange={event => setName(event.target.value)} required />
							</Form.Group>
							{/*Description Field*/}
							<Form.Group>
								<Form.Label>Description: </Form.Label>
								<Form.Control type="text" placeholder="Enter your product description" value={description} onChange={event => setDesc(event.target.value)} required />
							</Form.Group>
							{/*Price Field*/}
							<Form.Label>Price: </Form.Label>
							<InputGroup className="mb-4">
								<InputGroup.Text id="basic-addon1">&#8369;</InputGroup.Text>
								<Form.Control type="number" placeholder="Enter your price" value={price} onChange={event => setPrice(event.target.value)} required />
							</InputGroup>

							{/*Add Product Button*/}
							{
								isActive ? 
									<Button className="mb-3 btn-success btn-block" type="submit"> Add Product </Button>
								:
									<Button className="mb-3 btn-secondary btn-block" disabled> Add Product </Button>
							}
						</Form>
					</Container>
					</>
			}
		</>
	);
};