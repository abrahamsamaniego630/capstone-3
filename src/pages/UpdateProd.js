import React from 'react';

import { useState, useEffect, useContext } from 'react';
import { useParams, Link } from 'react-router-dom';
import { Container, Form, Button } from 'react-bootstrap';
import { Navigate }from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

import Hero from '../components/Banner';

const data = {
	title: 'Welcome to the Update Product Page',
	content: 'You can update details for a course here'
}

export default function Update() {

	const { user } = useContext(UserContext);

	const [name, setName] = useState('');
	const [description, setDesc] = useState('');
	const [price, setPrice] = useState('');
	
	const {id} = useParams();
	let token = localStorage.getItem('access');

	const [isActive, setIsActive] = useState(false);

	useEffect(() => {
		if (name !== null && description !== null && price !== null) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	},[name, description, price, id]);

	const updateProd = async(eventSubmit) => {
		eventSubmit.preventDefault()

		const isUpdated = await fetch(`https://evening-caverns-96869.herokuapp.com/products/${id}/update`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				"name": name,
				"description": description,
				"price": price
			})
		}).then(response => response.json())
		.then(data => {
			if (data) {
				return true;
			} else {
				return false;
			}
		})

		if (isUpdated) {
			setName('');
			setDesc('');
			setPrice('');

			await Swal.fire({
				icon: 'success',
				title: 'Update Successful',
				text: 'Product was successfully updated with the new details!' 
			});
			window.location.href = "/products";
		} else {
			Swal.fire({
				icon: 'error',
				title: 'Something Went Wrong',
				text: 'Try Again Later!'
			});
		}
	};

	return(
		<>
			{
				user.isAdmin !== true ?
					<Navigate to="/error" replace={true} />
				:
				<>
					<Hero bannerData={data} />
					<Container className="container-fluid form__view">
						<div>
							<h1 className="text-center mt-3">Update Product Form</h1>
							<Form onSubmit={e => updateProd(e)}>
								{/*Product Name Field*/}
								<Form.Group>
									<Form.Label>Product Name: </Form.Label>
									<Form.Control type="text" placeholder="Enter your product name" required value={name} onChange={e => {setName(e.target.value)}} />
								</Form.Group>
								{/*Description Field*/}
								<Form.Group>
									<Form.Label>Description: </Form.Label>
									<Form.Control type="text" placeholder="Enter a description here" required value={description} onChange={e => {setDesc(e.target.value)}} />
								</Form.Group>
								{/*Price*/}
								<Form.Group>
									<Form.Label>Price: </Form.Label>
									<Form.Control type="number" placeholder="Enter your price" required value={price} onChange={e => {setPrice(e.target.value)}} />
								</Form.Group>
								{/*Update Button*/}
								{
									isActive ? 
										<Button className="mb-3 btn-success btn-block card__btn" type="submit"> Update </Button>
									:
										<Button className="mb-3 btn-secondary btn-block card__btn" disabled> Update </Button>
								}
								<Link className="btn btn-block btn-success mr-3 mb-3 card__btn" to="/">
									Back to Home
								</Link>
							</Form>							
						</div>
					</Container>
				</>
			}
		</>
	);
};