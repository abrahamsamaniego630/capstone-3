import Bruce from './../components/Banner';
import UserContext from '../UserContext';

import { Card, Container } from 'react-bootstrap';
import { Navigate }from 'react-router-dom';

import { useState, useEffect, useContext } from 'react';
import { useParams, Link } from 'react-router-dom';

import Swal from 'sweetalert2';
import React from 'react';

let data = {
	content: 'Please see the product information below'
}

export default function ProdView(){

	const [prodInfo, setProdInfo] = useState({
		name: null,
		description: null,
		price: null
	})

	const [imgError, setImgError] = useState(false);

	const { user } = useContext(UserContext);	

	const {id} = useParams();
	let token = localStorage.getItem('access');
	let prod = {id}.id;

	if (user.isAdmin !== true) {
		data.title = '';
	} else {
		data.title = 'Welcome Admin User!';
	}

	useEffect(() => {
		fetch(`https://evening-caverns-96869.herokuapp.com/products/${id}/view`)
		.then(res => res.json())
		.then(convertedData => {
			setProdInfo({
				name: convertedData.name,
				description: convertedData.description,
				price: convertedData.price,
			})
		})
	},[id, prod])

	const addCart = () => {
		return(
			Swal.fire({
				icon: "success",
				title: 'Successfully Added to Cart!',
				text: 'Thank you for your order.'
			})
		);
	};
	const delProd = async(click) => {
		click.preventDefault()
		await Swal.fire({
			title: "Are you sure?",
			text: "You won't be able to revert this!",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, delete it!'
		}).then((result) => {
			if (result.isConfirmed) {
				fetch(`https://evening-caverns-96869.herokuapp.com/products/${id}/delete`, {
					method: 'DELETE',
					headers: {
						'Authorization': `Bearer ${token}`
					}
				}).then(response => response.json())
				.then(async data => {
					if (data) {
						click.preventDefault()
						await Swal.fire(
							'Deleted!',
							'Your file has been deleted.',
							'success'
						)
						window.location.href = "/";
						return true;
					} else {
						return false;
					}
				})
			}
		})
	}

	const arcProd = async(click) => {
		click.preventDefault()
		await Swal.fire({
			title: "Are you sure?",
			text: "Product will be archived, and can be reactivated again later.",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, archive it!'
		}).then((result) => {
			if (result.isConfirmed) {
				fetch(`https://evening-caverns-96869.herokuapp.com/products/${id}/archive`, {
					method: 'PUT',
					headers: {
						'Authorization': `Bearer ${token}`
					}
				}).then(response => response.json())
				.then(async data => {
					if (data) {
						click.preventDefault()
						await Swal.fire(
							'Archived!',
							'Your product has been archived.',
							'success'
						)
						window.location.href = "/";
						return true;
					} else {
						return false;
					}
				})
			}
		})
	}

		const actProd = async(click) => {
		click.preventDefault()
		await Swal.fire({
			title: "Are you sure?",
			text: "Product will be activated, and can be archived again later.",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, activate it!'
		}).then((result) => {
			if (result.isConfirmed) {
				fetch(`https://evening-caverns-96869.herokuapp.com/products/${id}/activate`, {
					method: 'PUT',
					headers: {
						'Authorization': `Bearer ${token}`
					}
				}).then(response => response.json())
				.then(async data => {
					if (data) {
						click.preventDefault()
						await Swal.fire(
							'Activated!',
							'Your product has been activated.',
							'success'
						)
						window.location.href = "/";
						return true;
					} else {
						return false;
					}
				})
			}
		})
	}

	return(
		<>
			{
				user.isAdmin !== true ?
				<Navigate to="/error" replace={true} />
				:
				<>
					<Bruce bannerData={data} />
						<Container className="container-fluid">
							<Card className="d-flex justify-content-center text-center">
								<Card.Body className="view__card">
									{
										!imgError ?
											<img className="card__imgview img-fluid" src={`/images/${prod}.png`} onError={() => setImgError(true)} alt="No screens available yet" />
										:
											<img className="card__imgview img-fluid" src="/images/imagenotfound.jpg" alt="Not Found" />
									}
									{/*Course Name*/}
									<Card.Title>
										<h2 className="mt-2"> {prodInfo.name} </h2>
									</Card.Title>

									{/*Course Description*/}
									<Card.Subtitle>
										<h6 className="my-4">Description: </h6>
									</Card.Subtitle>
									<Card.Text>
										{prodInfo.description}
									</Card.Text>

									{/*Course Price*/}
									<Card.Subtitle>
										<h6 className="my-4">Price: </h6>
									</Card.Subtitle>
									<Card.Text>
										&#8369; {prodInfo.price}
									</Card.Text>
									{user.isAdmin !== true ?
											<>
												<Link className="btn btn-large btn-success mr-3 mb-3 card__btn" to="/login" onClick={addCart}>
													Login to Buy
												</Link>
												<Link className="btn btn-large btn-success mr-3 mb-3 card__btn" to="/">
													Back to Home
												</Link>												
											</>
										: 
											<>
												<Link className="btn btn-large btn-success mr-3 mb-3 card__btn" to={`/products/update/${id}`}>
													Update Info
												</Link>
												<Link className="btn btn-large btn-success mr-3 mb-3 card__btn" to="/products" onClick={arcProd}>
													Archive Product
												</Link>
												<Link className="btn btn-large btn-success mr-3 mb-3 card__btn" to="/products" onClick={actProd}>
													Reactivate Product
												</Link>
												<Link className="btn btn-large btn-success mr-3 mb-3 card__btn" to="/products" onClick={delProd}>
													Delete Product
												</Link>
												<Link className="btn btn-large btn-success mr-3 mb-3 card__btn" to="/">
													Back to Home
												</Link>
											</>
									}
								</Card.Body>
							</Card>
						</Container>
				</>
			}

		</>
	);
};