import Bruce from './../components/Banner';
import UserContext from '../UserContext';

import { Card, Container } from 'react-bootstrap';

import { useState, useEffect, useContext } from 'react';
import { useParams, Link } from 'react-router-dom';

import Swal from 'sweetalert2';
import React from 'react';


let data = {
	content: 'Please see the product information below'
}

export default function UserView(){

	const [prodInfo, setProdInfo] = useState({
		name: null,
		description: null,
		price: null
	})

	const [imgError, setImgError] = useState(false);

	const { user } = useContext(UserContext);	

	const {id} = useParams();
	let prod = {id}.id;

	if (user.isAdmin !== true) {
		data.title = '';
	} else {
		data.title = 'Welcome Admin User!';
	}

	useEffect(() => {
		fetch(`https://evening-caverns-96869.herokuapp.com/products/${id}/view`)
		.then(res => res.json())
		.then(convertedData => {
			setProdInfo({
				name: convertedData.name,
				description: convertedData.description,
				price: convertedData.price,
			})
		})
	},[id, prod])

	const addCart = () => {
		return(
			Swal.fire({
				icon: "success",
				title: 'Successfully Added to Cart!',
				text: 'Thank you for your order.'
			})
		);
	};

	return(
		<>
			<Bruce bannerData={data} />
				<Container className="container-fluid mb-5">
					<Card className="d-flex justify-content-center text-center">
						<Card.Body className="view__card">
							{
								!imgError ?
									<img className="card__imgview img-fluid" src={`/images/${prod}.png`} onError={() => setImgError(true)} alt="No screens available yet" />
								:
									<img className="card__imgview img-fluid" src="/images/imagenotfound.jpg" alt="Not Found" />
							}
							{/*Course Name*/}
							<Card.Title>
								<h2 className="mt-2"> {prodInfo.name} </h2>
							</Card.Title>

							{/*Course Description*/}
							<Card.Subtitle>
								<h6 className="my-4">Description: </h6>
							</Card.Subtitle>
							<Card.Text>
								{prodInfo.description}
							</Card.Text>

							{/*Course Price*/}
							<Card.Subtitle>
								<h6 className="my-4">Price: </h6>
							</Card.Subtitle>
							<Card.Text>
								&#8369; {prodInfo.price}
							</Card.Text>
							{user.isAdmin !== false ?
									<>
										<Link className="btn btn-large btn-success mr-3 mb-3 card__btn" to="/">
											Login to Buy
										</Link>
										<Link className="btn btn-large btn-success mr-3 mb-3 card__btn" to="/">
											Back to Home
										</Link>										
									</>
								: 
									<>
										<Link className="btn btn-large btn-success mr-3 mb-3 card__btn" to="/" onClick={addCart}>
											Add to Cart
										</Link>
										<Link className="btn btn-large btn-success mr-3 mb-3 card__btn" to="/">
											Back to Home
										</Link>										
									</>
							}
						</Card.Body>
					</Card>
				</Container>
		</>
	);
};