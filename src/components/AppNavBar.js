import { useContext } from 'react';
import { Navbar, Nav, NavDropdown, Container } from 'react-bootstrap';
import { FaUser } from "react-icons/fa";
import { Link } from 'react-router-dom';

import UserContext from '../UserContext';

function AppNavBar() {
	const { user } = useContext(UserContext);
	return(
		<Navbar className="fixed-top test__nav" expand="lg">
			<Container className="container-fluid">
				<Navbar.Brand className="mt-3 branding"> <p>Coffi<b className="text__bold">Teastro</b></p> </Navbar.Brand>
				<Navbar.Toggle aria-controls="basic-navbar-nav" />
					<Navbar.Collapse className="text-center nav__collapse">
						<Nav className="ml-auto">
							<Link className="nav-link navi" to="/">
								Home
							</Link>
							{user.isAdmin === true ?
								<>
									<Link className="nav-link navi" to="/create">
										Add Product
									</Link>
									<Link className="nav-link navi" to="/products">
										View User Products
									</Link>
								</>
							:
								<Link className="nav-link navi" to="/products">
									Products
								</Link>
							}
							{user.id !== null ?
								<>
								<NavDropdown title={<FaUser />} id="basic-nav-dropdown">
									<NavDropdown.Item href="/password" className="dropdown">Update Password</NavDropdown.Item>
									<NavDropdown.Item href="/logout" className="dropdown">Logout</NavDropdown.Item>
								</NavDropdown>
								</>
							:
								<>
									<Link className="nav-link navi" to="/register">
										Register
									</Link>
									<Link className="nav-link navi" to="/login">
										Login
									</Link>
								</>
							}
						</Nav>
					</Navbar.Collapse>
			</Container>
		</Navbar>		
	);
};

export default AppNavBar;