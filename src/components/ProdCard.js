import React from 'react';
import UserContext from '../UserContext';

import { useState, useContext } from 'react';
import { Link } from 'react-router-dom';
import { Card, Container } from 'react-bootstrap';

export default function ProdCard({productProp}) {

	const { user } = useContext(UserContext);
	const [imgError, setImgError] = useState(false);

	return(
		<Card className="prod__card mb-3 d-inline-flex col-sm-12 col-md-8 col-lg-4 col-xl-3">
			<Container>
				{
					!imgError ?
						<img className="card__img" src={`./images/${productProp._id}.png`} onError={() => setImgError(true)} alt="No screens available yet" />
					:
						<img className="card__img" src="/images/imagenotfound.jpg" alt="Not Found" />
				}
				<Card.Body>
					<Card.Title>
						{productProp.name}
					</Card.Title>
					<Card.Text>
						{productProp.description}
					</Card.Text>
					<Card.Text>
						&#8369; {productProp.price}
					</Card.Text>
					{
						user.isAdmin !== true ?
						<Link to={`/products/view2/${productProp._id}`} className="card__btn btn btn-success">
							View Product
						</Link>
						:
						<Link to={`/products/view/${productProp._id}`} className="card__btn btn btn-success">
							View Product
						</Link>
					}
				</Card.Body>			
			</Container>
		</Card>						
	)
}

