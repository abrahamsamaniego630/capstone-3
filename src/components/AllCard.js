import React from 'react';

import { useState } from 'react';
import { Link } from 'react-router-dom';
import { Card, Container } from 'react-bootstrap';

export default function AllCard({allProp}) {

	const [imgError, setImgError] = useState(false);

	return(
		<Card className="prod__card mb-3 d-inline-flex col-sm-12 col-md-8 col-lg-4 col-xl-3">
			<Container>
				{
					!imgError ?
						<img className="card__img" src={`./images/${allProp._id}.png`} onError={() => setImgError(true)} alt="No screens available yet" />
					:
						<img className="card__img" src="/images/imagenotfound.jpg" alt="Not Found" />
				}
				<div>
					<Card.Body>
						<Card.Title>
							{allProp.name}
						</Card.Title>
						<Card.Text>
							{allProp.description}
						</Card.Text>
						<Card.Text>
							&#8369; {allProp.price}
						</Card.Text>
						<Link to={`/products/view/${allProp._id}`} className="card__btn btn btn-success">
							View Product
						</Link>						
					</Card.Body>				
				</div>
			</Container>
		</Card>
	)
}

