import {Row, Col, Container} from 'react-bootstrap';

export default function Banner({bannerData}) {

	return(
		<Container className="banner">
			<Row className="row mt-5">
				<Col className="col text-center">
					<p className="mt-4 text__banner1"> <strong className="text__bold">Welcome</strong> to Coffee<strong className="text__bold">Teastro!</strong> </p>
					<p className="text__banner2"> <strong> {bannerData.title} </strong> </p>
					<p className="my-2 text__banner3"> {bannerData.content} </p>
				</Col>
			</Row>			
		</Container>
	);
}
