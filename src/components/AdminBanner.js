import {Row, Col, Container } from 'react-bootstrap';

export default function AdminBanner() {
	return(
		<div className="text-center">
			<Container className="banner">
				<Row className="row mt-5">
					<Col className="col">
						<p className="mt-4 text__banner1"> <strong className="text__bold">Welcome</strong> to Coffee<strong className="text__bold">Teastro!</strong> </p>
						<p className="text__banner2"> Bringing <strong className="text__bold">Coffee Breaks</strong> and <b className="text__bold">Tea Spilling</b></p>
						<p className="text__banner3"> Anytime. Anywhere.</p>
					</Col>
				</Row>			
			</Container>
		</div>
	);
}
