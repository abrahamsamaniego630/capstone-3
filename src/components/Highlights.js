import {Row, Col, Card, Container} from 'react-bootstrap'
import { Link } from 'react-router-dom';

export default function Highlights() {
	return(
		<Container>
			<Row className="my-3">
				{/*1st Highlight*/}
				<Col className="mb-3" xs={12} md={4}>
					<Card className="p-4 cardHighlight">
						<Card.Body>
							<Link className="nav-link" to="./products/all"> View All Products </Link>
							<Card.Text>
								This option will allow you to view all products whether they are archived or not.
							</Card.Text>
						</Card.Body>
					</Card>			
				</Col>

				{/*2nd Highlight*/}
				<Col className="mb-3" xs={12} md={4}>
					<Card className="p-4 cardHighlight">
						<Card.Body>
							<Card.Title> View Available Products </Card.Title>
							<Card.Text>
								This option allows you to view all active or non-archived products.
							</Card.Text>
						</Card.Body>
					</Card>			
				</Col>

				{/*3rd Highlight*/}
				<Col className="mb-3" xs={12} md={4}>
					<Card className="p-4 cardHighlight">
						<Card.Body>
							<Card.Title> Update Password </Card.Title>
							<Card.Text>
								This option will direct you to the password change function.
							</Card.Text>
						</Card.Body>
					</Card>			
				</Col>
			</Row>
		</Container>

	)
}